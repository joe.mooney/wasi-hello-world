// Code from
// https://github.com/deislabs/krustlet/blob/master/docs/intro/tutorial01.md

use std::thread::sleep;
use std::time::Duration;

fn main() {
    loop {
        println!("Hello, from WASI and Krustlet!");
        sleep(Duration::from_secs(5));
    }
}
